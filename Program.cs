﻿using System;
using System.Collections.Generic;
using Microsoft.ML;

namespace MLdotNetTextClassification {
	class Program {
		static void Main(string[] args) {
			Console.WriteLine("Hello World!");

// https://www.youtube.com/watch?v=83LMXWmzRDM
// Questpond - ml.net C# machine learning tutorial for beginners

			List<TextWithInt> trainigData = new TrainingData().trainingData;

// https://docs.microsoft.com/de-de/dotnet/machine-learning/how-to-guides/load-data-ml-net

// https://docs.microsoft.com/de-de/dotnet/api/microsoft.ml.textcatalog.featurizetext?view=ml-dotnet

// https://www.youtube.com/watch?v=VJTpbZrBAeU

// Create MLContext
			MLContext mLContext = new MLContext();

// Load Data
			var dataView = mLContext.Data.LoadFromEnumerable<TextWithInt>(trainigData);

// Configuration before Training
			var pipeline = mLContext.Transforms.Text.FeaturizeText(outputColumnName: "Features", inputColumnName: "Text");

			IEstimator<ITransformer> processPipeline = pipeline
				.Append(mLContext.BinaryClassification.Trainers.SdcaLogisticRegression(labelColumnName: "LabelBool", featureColumnName: "Features"));

// Training
			var trainedModel = processPipeline.Fit(dataView);

// (Evaluate)
			var predictions = trainedModel.Transform(dataView);
			var metrics = mLContext.BinaryClassification.Evaluate(predictions, labelColumnName: "LabelBool", predictedLabelColumnName: "PredictedLabel");
			Console.WriteLine(metrics.Accuracy + " " + metrics.AreaUnderPrecisionRecallCurve + " " + metrics.F1Score);

// Test
			var predictior = mLContext.Model.CreatePredictionEngine<TextWithInt, PredictionWithProbability>(trainedModel);

			string s = default;
			while (s != "quit") {
				Console.WriteLine("Enter:");
				s = Console.ReadLine();
				var predResult = predictior.Predict( new TextWithInt(s));
				Console.WriteLine(s + " -> " + predResult.Prediction + " (" + predResult.Probability + ")");
			}
		}
	}
}
