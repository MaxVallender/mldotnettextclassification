using Microsoft.ML.Data;

namespace MLdotNetTextClassification {
	public class TextWithInt {
		[ColumnName("Text")]
		public string text { get; }

		[ColumnName("Label")]
		public int isGood { get; }

		[ColumnName("LabelBool")]
		//public bool isGoodBool() => ( (isGood >= 3) ? true : false );
		public bool isGoodBool { get; }

		public TextWithInt(string text, int isGood = 0) {
			this.text = text;
			this.isGood = isGood;
			this.isGoodBool = ( (isGood >= 3) ? true : false );
		}
	}

	public class PredictionWithProbability {
		[ColumnName("PredictedLabel")]
		public bool Prediction;
		public float Probability;
	}
}