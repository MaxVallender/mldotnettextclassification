using System.Collections.Generic;

namespace MLdotNetTextClassification {
	class TrainingData {
		public List<TextWithInt> trainingData;

		void initTrainingData() {
			trainingData = new List<TextWithInt>();

			trainingData.Add( new TextWithInt("Teste gerade das Programm. Es kann noch nichts!!", 0) );
			trainingData.Add( new TextWithInt("Naja, wohl ein Anfänger. Noch schlecht.", 1) );
			trainingData.Add( new TextWithInt("Warum so kritisch. Es wird bestimmt super mega hammer geil!", 4) );
			trainingData.Add( new TextWithInt("Stiftung Datentest: befriedigt, bis heiter.", 1) );
			trainingData.Add( new TextWithInt("Passabler Anfang.", 3) );
			trainingData.Add( new TextWithInt("Funktioniert nicht!", 1) );
			trainingData.Add( new TextWithInt("Compiler vermeldet keine Probleme mehr.", 3) );
			trainingData.Add( new TextWithInt("Interpreter läuft auch. Gut.", 4) );
			trainingData.Add( new TextWithInt("Super.", 5) );
			trainingData.Add( new TextWithInt("Noch so ein sinnloser Kommentar. Finde ich nicht gut", 2) );
			trainingData.Add( new TextWithInt("Mir gefällt es ..vielleicht.", 3) );
			trainingData.Add( new TextWithInt("Aha, hier wird trainiert. Gut, dann weiter so!", 4) );
			trainingData.Add( new TextWithInt("Ahso, ist nur training. Na dann", 3) );
			trainingData.Add( new TextWithInt("Es sind doch lediglich Beispielsätze! Unseriös.", 2) );
			trainingData.Add( new TextWithInt("Mir fällt kaum mehr was ein. Das ist schlecht.", 1) );
			trainingData.Add( new TextWithInt("Warum nicht noch einen? Geht doch!", 4) );
			trainingData.Add( new TextWithInt("Gut.", 4) );
			trainingData.Add( new TextWithInt("Sehr gut.", 5) );
			trainingData.Add( new TextWithInt("Gefällt mir.", 4) );
			trainingData.Add( new TextWithInt("Gefällt mir nicht.", 1) );
			trainingData.Add( new TextWithInt("Schlecht", 1) );
			trainingData.Add( new TextWithInt("Sehr gute Software. Kaufe alles!", 5) );
			trainingData.Add( new TextWithInt("Für 10.999€ keine Kaufempfehlung. Auch nicht an BlackFriday.", 1) );
			trainingData.Add( new TextWithInt("Mir gefällt es gut.", 4) );
			trainingData.Add( new TextWithInt("Coole Beispiele.", 4) );
			trainingData.Add( new TextWithInt("Schlechte Rächtschreibing!", 2) );
			trainingData.Add( new TextWithInt("Für meinen Ansprüchen reicht es.", 3) );
			trainingData.Add( new TextWithInt("Genügt meinen Ansprüchen nicht.", 2) );
			trainingData.Add( new TextWithInt("Gut ist es nicht.", 3) );
			trainingData.Add( new TextWithInt("Schlecht aber auch nicht.", 4) );
			
			trainingData.Add( new TextWithInt("Selbst ein Stern wäre zuviel!!", 0) );
			trainingData.Add( new TextWithInt("Nur zwei Sterne", 2) );
			trainingData.Add( new TextWithInt("Gerade noch so, drei Sterne", 3) );
			trainingData.Add( new TextWithInt("Vier Sterne", 4) );
			trainingData.Add( new TextWithInt("Dafür gibt es fünf Sterne", 5) );
		}

		public TrainingData() {
			initTrainingData();
		}
	}
}